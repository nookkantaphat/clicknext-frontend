import { Button } from '@nextui-org/button';
import { Card, CardBody, CardFooter, CardHeader, Divider, Image, Input, Link } from '@nextui-org/react';

export default function Register() {
    return (
        <div className="container">
            <Card className="max-w-[400px] m-auto mt-10">
                <CardHeader className="flex gap-3">
                    {/* <Image
                        alt="nextui logo"
                        height={40}
                        radius="sm"
                        src="https://avatars.githubusercontent.com/u/86160567?s=200&v=4"
                        width={40}
                    /> */}
                    <div className="flex flex-col">
                        <p className="text-md">Register</p>
                        {/* <p className="text-small text-default-500">nextui.org</p> */}
                    </div>
                </CardHeader>
                <Divider />
                <CardBody className='flex' style={{paddingLeft: '2.5em',paddingTop:'2.5em'}}>
                    <form>
                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="text"
                                label="Username"
                                variant="bordered"
                                // placeholder="Enter your username"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>
                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="password"
                                label="Password"
                                variant="bordered"
                                // placeholder="Enter your password"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>
                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="password"
                                label="Confirm Password"
                                variant="bordered"
                                // placeholder="Enter your Confirm password"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>
                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="email"
                                label="Email"
                                variant="bordered"
                                // placeholder="Enter your email"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>

                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="test"
                                label="Firstname"
                                variant="bordered"
                                // placeholder="Enter your email"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>

                        <div className="flex w-full flex-wrap md:flex-nowrap mb-6 ">
                            <Input
                                // isClearable
                                type="text"
                                label="Lastname"
                                variant="bordered"
                                // placeholder="Enter your email"
                                defaultValue=""
                                // onClear={() => console.log("input cleared")}
                                className="max-w-xs"
                            />
                        </div>



                    </form>
                </CardBody>
                {/* <Divider /> */}
                {/* <CardFooter>
                    <Link
                        isExternal
                        showAnchorIcon
                        href="https://github.com/nextui-org/nextui"
                    >
                        Visit source code on GitHub.
                    </Link>
                </CardFooter> */}
            </Card>
        </div>

    )
}