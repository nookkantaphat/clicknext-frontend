'use client'

import { Button } from '@nextui-org/button';
import { Avatar, Card, CardBody, CardFooter, CardHeader, Divider, Dropdown, DropdownItem, DropdownMenu, DropdownTrigger, Image, Input, Link, Listbox, ListboxItem, ListboxSection, Navbar, NavbarBrand, NavbarContent, NavbarItem } from '@nextui-org/react';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc'
import { useEffect, useRef, useState } from 'react';
import { io } from 'socket.io-client';
import { loadEnvConfig } from '@next/env'
dayjs.extend(utc)
// const socket = io("http://localhost:3001", { transports: ['websocket'] });

function Chat() {

    const messagesEndRef = useRef<any>(null);
    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" })
    }
    const [socket, setSocket] = useState<any>(null);
    const [account, setAccount] = useState<any>([])
    const [message, setMessage] = useState<any>([])
    const [inputMessage, setInputMessage] = useState<String>('')
    const [currentUser, setCurrentUser] = useState<any>(null)
    useEffect(() => {
        const s = io(`${process.env.WEBSOCKET_URL}`, { transports: ['websocket'] });
        s.on('message', (data: any) => {
            updateMessage(data)

        })
            s.on('update-account', (data: any) => {
                loadAccount()
            }) 
        return () => {
            s.disconnect();
        };

    }, [setSocket]);


    useEffect(() => {
        scrollToBottom()
    }, [message]);
    const updateMessage = (data: any) => {
        const user = localStorage.getItem("user")
        if (user) {
            if (data.account['_id'] == user) {
                const message_item = [data.data]
                setMessage((current: any) => [...current, ...message_item]);
            }
        }
    }
    const changeUser = async (userInfo: any) => {
        setCurrentUser(userInfo)
        loadAccountMessage(userInfo._id)
        localStorage.setItem("user", userInfo._id)
    }
    // console.log(currentUser)
    const loadAccount = async () => {
        fetch(
            `${process.env.BACKEND_URL}/account/list`, { method: 'GET' }
        ).then((res) => res.json())
            .then((res) => {
                setAccount(res.result);
                const user = localStorage.getItem("user")
                if (user) {
                    const u = res.result.filter((x: any) => x._id == user)
                    if (u && u.length > 0) {
                        setCurrentUser(u[0])
                        loadAccountMessage(u[0]._id)
                    }else{
                        localStorage.removeItem("user")
                        setCurrentUser(null)
                        setMessage([])
                    }

                }
            });
    }

    const loadAccountMessage = async (user_id: string) => {
        fetch(
            `${process.env.BACKEND_URL}/line/message?user_id=${user_id}`, { method: 'GET' }
        ).then((res) => res.json())
            .then((res) => {
                console.log('message-load', res.result.message)
                setMessage(res.result.message);
            });
    }
    useEffect(() => {
        loadAccount()

    }, [])
  

    const onKeyDown = (e: any) => {
        // 'keypress' event misbehaves on mobile so we track 'Enter' key via 'keydown' event
        if (e.keyCode === 13) {
            if (!currentUser) alert('no user')
            e.preventDefault();
            onSubmit(e);

        }

    }

    const onSubmit = (e: any): void => {
        if (inputMessage) {
            fetch(
                `${process.env.BACKEND_URL}/line/send-message`,
                {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        "message": inputMessage,
                        "user_id": currentUser._id
                    })
                }

            ).then((res) => res.json())
                .then((res) => {
                    console.log('message', res.result)
                    if (res && res.result) {
                        const message_item = [res.result]
                        console.log('mmmm', message_item)
                        // setMessage((current: any) => [...current, ...message_item]);
                    }

                });
            //    alert(inputMessage)
            setInputMessage('')
            e.target.value = '';

        }
    }

    // console.log('mmm', message)

    return (
        <div className="h-full w-full">
            <Navbar className='w-full fixed' style={{ borderBottom: '1px solid #ddd' }}>
                <NavbarBrand>
                    {/* <AcmeLogo /> */}
                    <p className="font-bold text-inherit">CHAT</p>
                </NavbarBrand>

                <NavbarContent as="div" justify="end" >
                    <Dropdown placement="bottom-end">
                        <DropdownTrigger>
                            <Avatar
                                isBordered
                                as="button"
                                className="transition-transform"
                                color="secondary"
                                name="Jason Hughes"
                                size="sm"
                                src="https://i.pravatar.cc/150?u=a042581f4e29026704d"
                            />

                        </DropdownTrigger>
                        <DropdownMenu aria-label="Profile Actions" variant="flat">
                            <DropdownItem key="logout" color="danger">
                                Log Out
                            </DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                </NavbarContent>
            </Navbar>
            <Divider />
            {/* <div>
                <Button onClick={loadAccount}>Click me</Button>
            </div> */}
            <div className="flex grid-cols-4 gap-4" style={{ height: 'calc(100% - 80px)', marginTop: '68px' }}>

                <div className="basis-1/4 h-full w-10">
                    <Listbox variant="flat" aria-label="Listbox menu with sections">
                        {
                            account?.map((item: any, index: number) => {
                                return (
                                    <ListboxSection showDivider key={index}>
                                        <ListboxItem onClick={() => changeUser(item)} key="new" description={item.firstname} >
                                        </ListboxItem>
                                    </ListboxSection>)

                            })
                        }
                    </Listbox>

                </div>
                <div className="basis-1/2 col-span-2 " style={{ borderRight: '1px solid #ddd', borderLeft: '1px solid #ddd' }}>
                    <div style={{ width: '100%', padding: '10px', height: 'calc(100% - 70px)', overflowY: 'auto' }} >
                        {
                            message?.map((item: any, index: number) => {
                                if (item.user_type == 'user') {
                                    return (<div className='w-100' style={{ textAlign: 'left', marginTop: 30 }} key={index}>
                                        <span style={{ backgroundColor: '#dddddd', padding: '10px 20px', borderRadius: 10 }}>{item.message_text}</span>
                                        <span style={{ fontSize: 11, paddingLeft: 10 }}>{dayjs(dayjs.unix(item.message_timestamp / 1000)).format('YYYY-MM-DD HH:mm')}</span>
                                    </div>)
                                } else {
                                    return (<div key={index} className='w-100' style={{ textAlign: 'right', marginTop: 30 }} >
                                        <span style={{ fontSize: 11, paddingRight: 10 }}>{dayjs(dayjs.unix(item.message_timestamp / 1000)).format('YYYY-MM-DD HH:mm')}</span>
                                        <span style={{ backgroundColor: '#dddddd', padding: '10px 20px', borderRadius: 10 }}>{item.message_text}</span>

                                    </div>)
                                }

                            })
                        }
                        <div style={{ marginBottom: 50 }} ref={messagesEndRef} />

                    </div>
                    <div style={{ padding: 10 }}>
                        <Input
                            type="text"
                            label="Message"
                            labelPlacement="inside"
                            onChange={e => { setInputMessage(e.currentTarget.value); }}
                            onKeyDown={(e) => onKeyDown(e)}
                            value={inputMessage.toString()}
                        // endContent={
                        //     <div className="pointer-events-none flex items-center">
                        //         <span className="text-default-400 text-small"></span>
                        //     </div>
                        // }
                        />
                    </div>

                </div>
                <div className="basis-1/4" style={{ paddingTop: 10 }}>
                    {
                        currentUser ? <div className="w-100">
                            <Image
                                width={240}
                                style={{ margin: 'auto', textAlign: 'center', display: 'flex' }}
                                alt="NextUI Fruit Image with Zoom"
                                src="https://nextui-docs-v2.vercel.app/images/fruit-1.jpeg"
                            />
                            <div> {currentUser.firstname}  {currentUser.lastname}</div>
                            <Button color="primary" variant="ghost">
                                Block
                            </Button>
                        </div> : <></>
                    }

                </div>
            </div>
        </div>

    )
}

export default Chat