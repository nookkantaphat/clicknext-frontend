/** @type {import('next').NextConfig} */
require("dotenv").config();
const nextConfig = {
    webpack: (config, { isServer }) => {
        isServer && (config.externals = [...config.externals, 'socket.io-client']);
        return config;
    },
    reactStrictMode: false,
    env: {
        BACKEND_URL: process.env.BACKEND_URL,
        WEBSOCKET_URL:process.env.WEBSOCKET_URL,
        BASE_URL: process.env.BASE_URL
      },
}

module.exports = nextConfig
